FROM centos:7

RUN yum -y update
RUN yum install -y vsftpd

COPY vsftpd.conf /etc/

RUN mkdir -p /var/ftp
RUN mkdir -p /var/log
RUN useradd -m lappis
RUN echo -e "lappis\nlappis" | passwd lappis
RUN chown -R lappis:lappis /var/ftp

VOLUME /var/ftp/
VOLUME /var/log/vsftpd

EXPOSE 20 21 40000

COPY run-vsftpd.sh /usr/sbin/
RUN chmod +x /usr/sbin/run-vsftpd.sh
CMD ["/usr/sbin/run-vsftpd.sh"]
